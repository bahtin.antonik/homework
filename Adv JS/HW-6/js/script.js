const btn = document.querySelector('.button');
const wrapper = document.querySelector('.wrapper');
btn.addEventListener('click', makeRequest);

async function makeRequest() {
  try {
    const response = await fetch('https://api.ipify.org/?format=json');
    const getIp = await response.json();

    const res = await fetch(
      `http://ip-api.com/json/${getIp.ip}?fields=continent,district,country,region,regionName,city`
    );
    const getPhysicIp = await res.json();

    siteStructure(getPhysicIp);
  } catch (e) {
    console.error(e);
  }
}
function siteStructure(ip) {
  const html = document.createElement('div');
  html.innerHTML = `
  <p>Continent: ${ip.continent}</p>
  <p>Country: ${ip.country}</p>
  <p>Region: ${ip.regionName} ${ip.region}</p>
  <p>City: ${ip.city}</p>
  <p>District: ${ip.district}</p>
  `;
  wrapper.append(html);
}
/*Теоретичне питання
Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
Асинхронний код однопотоковий, результат асинхронного коду отримується по мірі його закінчення.
Можна запустити тривале завдання та мати можливість редагувати інші події замість закінчення виконання довгого завдання.
*/
