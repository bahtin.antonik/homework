const newsFeed = document.querySelector('.news-feed');
async function getPosts() {
  try {
    const usersResponse = await fetch('https://ajax.test-danit.com/api/json/users');
    const users = await usersResponse.json();

    const postsResponse = await fetch('https://ajax.test-danit.com/api/json/posts');
    const posts = await postsResponse.json();
    users.forEach((user) => {
      posts.forEach((post) => {
        if (user.id === post.userId) {
          const card = new Card(post.title, post.body, user.name, user.username, user.email, post.id);
          card.render();
        }
        const cardDiv = document.querySelector('.card__button');
        cardDiv.addEventListener('click', deletePost);
      });
    });
  } catch (e) {
    console.error(e);
  }
}

async function deletePost(e) {
  const cardEl = e.target.closest('.card');

  await fetch(`https://ajax.test-danit.com/api/json/posts/${cardEl.dataset.id}`, { method: 'DELETE' }).then((r) => {
    if (r.status === 200 || r.ok === true) {
      cardEl.remove();
    }
  });
}

class Card {
  constructor(title, textContent, fullName, userName, email, id) {
    this.title = title;
    this.textContent = textContent;
    this.fullName = fullName;
    this.userName = userName;
    this.email = email;
    this.id = id;
  }

  render() {
    const siteStructure = `
    <div class="card" data-id="${this.id}">
        <span class="card__user-info">${this.fullName} &emsp; ${this.email}</span>
        <button class="card__button"> X </button>
        <span class="card__title">${this.title}</span>
        <p class="card__text">${this.textContent}</p>
    </div>
    `;
    newsFeed.insertAdjacentHTML('afterbegin', siteStructure);
  }
}

getPosts();
