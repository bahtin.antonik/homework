const books = [
  {
    author: 'Люсі Фолі',
    name: 'Список запрошених',
    price: 70,
  },
  {
    author: 'Сюзанна Кларк',
    name: 'Джонатан Стрейндж і м-р Норрелл',
  },
  {
    name: 'Дизайн. Книга для недизайнерів.',
    price: 70,
  },
  {
    author: 'Алан Мур',
    name: 'Неономікон',
    price: 70,
  },
  {
    author: 'Террі Пратчетт',
    name: 'Рухомі картинки',
    price: 40,
  },
  {
    author: 'Анґус Гайленд',
    name: 'Коти в мистецтві',
  },
];

const rootEl = document.querySelector('#root');

function errHandler(object) {
  const keys = ['author', 'name', 'price'];
  keys.forEach((key) => {
    if (!(key in object)) {
      throw new TypeError(`: ${key} is not defined`);
    }
  });
}

function ulCreate() {
  const ulList = document.createElement('ul');

  const newBooks = books.map((book) => {
    try {
      errHandler(book);
      return `
  <li>
    <p>Автор: ${book.author}</p>
    <p>Назва: ${book.name}</p>
    <p>Ціна: ${book.price}</p>
  </li>
  `;
    } catch (e) {
      console.error(e.name + e.message);
    }
  });

  ulList.innerHTML = newBooks.join('');
  rootEl.append(ulList);
}

ulCreate();
