class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  get age() {
    return this._age;
  }
  get salary() {
    return this._salary;
  }

  set name(name) {
    this._name = name;
  }
  set age(age) {
    this._age = age;
  }
  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }

  showInfo() {
    console.log(`Name: ${this.name}, age ${this.age}
salary: ${this.salary}
languages: ${this._lang}`);
  }
}

firstProg = new Programmer('Anton', 26, 7000, 'english, german');
secondProg = new Programmer('Danil', 25, 9000, 'english, ukrainian');

firstProg.showInfo();
secondProg.showInfo();

/*
Теоретичне питання

Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
Прототип це об'єкт від якого успадковуються інші об'єкти
Прототипне наслідування дає доступ до властивостей об'єкта

Для чого потрібно викликати super() у конструкторі класу-нащадка?
Викликати super() потрібно для успадкування властивостей батьківсього конструктору
 */
