const response = fetch('https://ajax.test-danit.com/api/swapi/films')
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    renderFilms(data);
  })
  .catch((err) => console.log(err));

function renderFilms(films) {
  films.forEach((element) => {
    const filmBlock = document.createElement('div');
    const characterList = document.createElement('ul');
    const filmName = document.createElement('p');
    const episodeID = document.createElement('p');
    const filmDescription = document.createElement('p');

    filmName.innerText = element.name;
    episodeID.innerText = element.episodeId;
    filmDescription.innerText = element.openingCrawl;
    filmBlock.append(filmName);
    filmBlock.append(episodeID);
    filmBlock.append(filmDescription);
    document.body.append(filmBlock);
    filmBlock.append(characterList);

    const filmCharacters = Promise.all(
      element.characters.map((character) => {
        return fetch(character).then((res) => res.json());
      })
    );
    filmCharacters.then((res) =>
      res.forEach((elem) => {
        const characterListItem = document.createElement('li');
        characterListItem.innerText = elem.name;
        characterList.append(characterListItem);
      })
    );
  });
}
