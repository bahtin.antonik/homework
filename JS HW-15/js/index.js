let userNumber;
do {
  userNumber = Number(prompt('Enter your factorial', userNumber));

} while (
  Number.isNaN(userNumber) || !userNumber
)

function factorialCounter(userNumber) {
  if (userNumber === 1) {
    return userNumber;
  }
  else {
    return userNumber * factorialCounter(userNumber - 1);
  }
}
alert(factorialCounter(userNumber));

/*
Теоретичні питання
Напишіть, як ви розумієте рекурсію.
Навіщо вона використовується на практиці?

Рекурсія це коли функція викликає сама себе.
В деяких випадках рекурсія спрощує логічне сприйняття рішення задачі.
*/