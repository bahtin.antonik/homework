const btnStart = document.querySelector('.btn-start');
const btnEnd = document.querySelector('.btn-end');
const imgCollection = document.querySelectorAll('.image');
let temp;
let index = 0;

clearInterval(temp);
temp = setInterval(displayImages, 3000);

function displayImages() {
  imgCollection[index === 0 ? imgCollection.length - 1 : index - 1].classList.remove('showImage');
  imgCollection[index].classList.add('showImage');
  index++;
  if (index >= imgCollection.length) {
    index = 0;
  }
}

btnEnd.addEventListener('click', () => {
  clearInterval(temp);
  btnStart.disabled = false;
});

btnStart.addEventListener('click', () => {
  btnStart.disabled = true;
  clearInterval(temp);
  temp = setInterval(displayImages, 3000);
});

/*
Теоретичні питання
Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
setTimeout() викликається одноразово, а setInterval() викликається ітеративно кожен раз по заданих мс.

Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
вона буде намагатися виконати функцію якумога швидше але тільки після виконання поточного коду

Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
щоб ітеративні запуски інтервалу не відбулись знову
 */
