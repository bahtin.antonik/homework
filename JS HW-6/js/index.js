function createNewUser() {
  const firstName = prompt("What is your name?");
  const lastName = prompt("What is your last name?");
  const birthday = prompt("What is your birthday date?");

  const validBirthday = birthday.split('.')

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    birthday: birthday,
    getLogin: function () {
      const lowercase = firstName.charAt(0) + lastName;
      const result = lowercase.toLowerCase();
      return result;
    },
    getAge: function () {
      const userAge = new Date().getFullYear() - validBirthday[2]
      return userAge;
    },
    getPassword: function () {
      const pass = (firstName.charAt(0)).toUpperCase() + lastName.toLowerCase() + validBirthday[2];
      return pass;
    }
  }
  return newUser;
};


const user = createNewUser()
console.log(user.getAge());
console.log(user.getPassword());
