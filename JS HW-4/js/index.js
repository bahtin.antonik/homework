let n;
let m;
let operator;
let result;

function mathOperations(n, m, operator) {
  do {
    n = Number(prompt("Enter your n number", n));
    m = Number(prompt("Enter your m number", m));
    operator = prompt("Enter your operator");
  } while (!n || !m || (operator !== "+" && operator !== "-" && operator !== "*" && operator !== "/"));
  if (operator === "+") {
    return n + m;
  } else if (operator === "-") {
    return n - m;
  } else if (operator === "*") {
    return n * m;
  } else if (operator === "/") {
    return n / m;
  }
}

console.log(mathOperations());

/*
Теоретичні питання

1. Описати своїми словами навіщо потрібні функції у програмуванні.

Функції потрібні для використання одних і тих самих дій багаторазово створивши всього одну функцію.
Це сильно скорочує сам код, тим самим його легше читати.

2. Описати своїми словами, навіщо у функцію передавати аргумент.
Функція приймає у себе аргумент та може оперувати з  отриманним значенням.

3. Що таке оператор return та як він працює всередині функції? 
Як тільки виконання функції доходить до "return",  то функція закінчує виконання та виходить із неї,
якщо дописати до "return" якісь значення або матетична операція, то він поверне її.
*/
