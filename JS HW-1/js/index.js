// Task 1
let admin;
const name = "Anton";

admin = name;

console.log(admin);

// Task 2
const days = 7;

if (days <= 10 && days >= 1) {
  console.log(days * 24 * 60 * 60);
}

// Task 3
const number = Number(prompt("Enter your number"));
console.log(number);

/* 
  Теоретичні питання

Які існують типи даних у Javascript?

number & BigInt & Boolean & String & null & undefined & object

У чому різниця між == і ===?

"==" - прирівнює  значення без суворого  приведення типів
"===" - прирівнює значення тільки з однаковим типом даних

Що таке оператор?

Оператор проводить операції з операндами.
Оператори бувають унарні, бінарні та тернарні.
приклади операторів: + - * / % > < => =< 

*/
