
function filterBy(userArray, userDataType) {
  const results = userArray.filter(item => typeof item !== userDataType)
  return results;
};

console.log(filterBy(['hello', 'world', 23, '23', null], "string"));
console.log(filterBy(['hello', 'world', 23, '23', null], "number"));
console.log(filterBy(['hello', 'world', 23, '23', null], "object"));

/*
Теоретичні питання
1. Опишіть своїми словами як працює метод forEach.
Метод forEach перебирає кожен елемент масиву і застосовує до
нього функцію callback з відповідними параметрами.

2. Як очистити масив?
Найкращий спобіб очистити масив це задати йому довжину 0,
тобто arr.length = 0;

3. Як можна перевірити, що та чи інша змінна є масивом?
Застосувати метод Array.isArray()
*/