const selectTabs = document.querySelector('.tabs');
const tabsContent = document.querySelectorAll('.tabs-content li');
selectTabs.addEventListener('click', (e) => {
  const li = e.target.closest('li');
  if (!li) {
    return;
  }
  const active = document.querySelector('.active');
  if (active) {
    active.classList.remove('active');
  }
  li.classList.add('active');
  tabsContent.forEach((liItem) => {
    liItem.classList.remove('active-tab-content');
    if (li.dataset.tabsTitle === liItem.dataset.tabsContent) {
      console.log(liItem.dataset.tabsContent);
      liItem.classList.add('active-tab-content');
    }
  });
});
