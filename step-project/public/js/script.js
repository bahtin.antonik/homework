// TABS IN SECTION "OUR SERVICE"
const selectTabs = document.querySelector('.tabs');
const tabsContent = document.querySelectorAll('.tabs-content li');

const selectTabsChild = document.querySelector('.tabs-title.active');
const selectTabsContentChild = tabsContent[0];

if (selectTabsChild.dataset.tabsTitle === selectTabsContentChild.dataset.tabsContent) {
  selectTabsContentChild.classList.add('active-tab-content');
}

selectTabs.addEventListener('click', (e) => {
  const li = e.target.closest('li');
  if (!li) {
    return;
  }
  const active = document.querySelector('.active');
  if (active) {
    active.classList.remove('active');
  }
  li.classList.add('active');
  tabsContent.forEach((liItem) => {
    liItem.classList.remove('active-tab-content');
    if (li.dataset.tabsTitle === liItem.dataset.tabsContent) {
      liItem.classList.add('active-tab-content');
    }
  });
});

//TABS IN SECTION "OUR AMAZING WORK"
const imgSortArr = [
  {
    category: 'Web Design',
    images: [
      './img/web-design/web-design1.jpg',
      './img/web-design/web-design2.jpg',
      './img/web-design/web-design3.jpg',
      './img/web-design/web-design4.jpg',
      './img/web-design/web-design5.jpg',
      './img/web-design/web-design6.jpg',
      './img/web-design/web-design7.jpg',
    ],
  },
  {
    category: 'Graphic Design',
    images: [
      './img/graphic-design/graphic-design1.jpg',
      './img/graphic-design/graphic-design2.jpg',
      './img/graphic-design/graphic-design3.jpg',
      './img/graphic-design/graphic-design4.jpg',
      './img/graphic-design/graphic-design5.jpg',
      './img/graphic-design/graphic-design6.jpg',
      './img/graphic-design/graphic-design7.jpg',
      './img/graphic-design/graphic-design8.jpg',
      './img/graphic-design/graphic-design9.jpg',
      './img/graphic-design/graphic-design10.jpg',
      './img/graphic-design/graphic-design11.jpg',
      './img/graphic-design/graphic-design12.jpg',
    ],
  },
  {
    category: 'Landing Pages',
    images: [
      './img/landing-page/landing-page1.jpg',
      './img/landing-page/landing-page2.jpg',
      './img/landing-page/landing-page3.jpg',
      './img/landing-page/landing-page4.jpg',
      './img/landing-page/landing-page5.jpg',
      './img/landing-page/landing-page6.jpg',
      './img/landing-page/landing-page7.jpg',
    ],
  },
  {
    category: 'Wordpress',
    images: [
      './img/wordpress/wordpress1.jpg',
      './img/wordpress/wordpress2.jpg',
      './img/wordpress/wordpress3.jpg',
      './img/wordpress/wordpress4.jpg',
      './img/wordpress/wordpress5.jpg',
      './img/wordpress/wordpress6.jpg',
      './img/wordpress/wordpress7.jpg',
      './img/wordpress/wordpress8.jpg',
      './img/wordpress/wordpress9.jpg',
      './img/wordpress/wordpress10.jpg',
    ],
  },
];
const imgArrayContainer = document.querySelector('.our-work-img-container');

const ourWorkTabs = document.querySelector('.our-work-tabs');
ourWorkTabs.addEventListener('click', sortCategoryBy);

function getSortedImagesByCategory(category) {
  let count = 0;
  let sortedImagesByCategory = [];
  imgSortArr.forEach((obj) => {
    if (category === obj.category) {
      sortedImagesByCategory = obj.images.map((imageSrc) => {
        return `
        <div class="flip-card" data-img-index=${count++}>
      <div class="flip-card-inner">
        <div class="flip-card-front">
        <img src=${imageSrc} data-category="${obj.category}" alt="Our work example"/>
        </div>
        <div class="flip-card-back">
          <div class="flip-card-back-img-container"> 
            <img src="./img/icons/Group-5.svg"/>
            <img src="./img/icons/Group-6.svg"/>
          </div>
          <p>CREATIVE DESIGN</p> 
          <h1>${obj.category}</h1> 
        </div>
      </div>
    </div>
      `;
      });
    }
  });
  return sortedImagesByCategory;
}

function sortCategoryBy(e) {
  const li = e.target.closest('li');
  if (!li) {
    return;
  }
  const activeSortTab = document.querySelector('.active-sort-tab');

  if (activeSortTab) {
    activeSortTab.classList.remove('active-sort-tab');
  }

  loadMoreBtn.classList.remove('load-more-btn-hidden');
  li.classList.add('active-sort-tab');
  imgArrayContainer.innerHTML = '';

  if (li.dataset.category === 'all') {
    const allImages = getAllImages();
    const cutImagesArr = allImages.filter((image, idx) => idx < 12);
    imgArrayContainer.insertAdjacentHTML('afterbegin', cutImagesArr.join(''));
  } else {
    const sortedArr = getSortedImagesByCategory(li.dataset.category);

    if (sortedArr.length <= 12) {
      loadMoreBtn.classList.add('load-more-btn-hidden');
    }
    imgArrayContainer.insertAdjacentHTML('afterbegin', sortedArr.join(''));
  }
}

function getAllImages() {
  let count = 0;
  const allImages = imgSortArr.map((obj) => {
    return obj.images.map((imageSrc) => {
      return `
      <div class="flip-card" data-img-index=${count++}>
      <div class="flip-card-inner">
        <div class="flip-card-front">
        <img src=${imageSrc} data-category="${obj.category}" alt="Our work example"/>
        </div>
        <div class="flip-card-back">
          <div class="flip-card-back-img-container"> 
            <img src="./img/icons/Group-5.svg"/>
            <img src="./img/icons/Group-6.svg"/>
          </div>
          <p>CREATIVE DESIGN</p> 
          <h1>${obj.category}</h1> 
        </div>
      </div>
    </div>
      `;
    });
  });
  const arr = [];
  allImages.forEach((elem) => {
    arr.push(...elem);
  });
  return arr;
}
let allImages = getAllImages();
const activeAllSortTab = document.querySelector('.sort-tabs');

if (activeAllSortTab.dataset.category === 'all') {
  allImages = getAllImages();
  const cutImagesArr = allImages.filter((image, idx) => idx < 12);
  imgArrayContainer.insertAdjacentHTML('afterbegin', cutImagesArr.join(''));
}

activeAllSortTab.classList.add('active-sort-tab');
const loadMoreBtn = document.querySelector('.load-more-btn');
loadMoreBtn.addEventListener('click', addMoreImages);

function addMoreImages(e) {
  const loadMoreBtn = document.querySelector('.load-more-btn');
  loadMoreBtn.innerHTML = '<i class="fa load-more-btn-icon fa-spinner fa-spin"></i> <span>Loading</span>';
  const ourWorkImgContainer = document.querySelector('.our-work-img-container');
  const ourWorkImgLastChild = ourWorkImgContainer.lastElementChild;
  setTimeout(() => {
    if (ourWorkImgLastChild.dataset.imgIndex === '23') {
      const thirdArr = allImages.slice(24, 36);
      imgArrayContainer.insertAdjacentHTML('beforeend', thirdArr.join(''));
      loadMoreBtn.innerHTML = `<img class="load-more-btn-icon" src="./img/icons/Forma 1.svg" alt="plus" />
      <span>LOAD MORE</span>`;
      loadMoreBtn.classList.add('load-more-btn-hidden');
    }
    if (ourWorkImgLastChild.dataset.imgIndex === '11') {
      const secondArr = allImages.slice(12, 24);
      imgArrayContainer.insertAdjacentHTML('beforeend', secondArr.join(''));
      loadMoreBtn.innerHTML = `<img class="load-more-btn-icon" src="./img/icons/Forma 1.svg" alt="plus" />
      <span>LOAD MORE</span>`;
    }
  }, 1000);
}

//about the ham

const bulletIconContainer = document.querySelector('.bullet-icons-container');
const bulletIconItems = document.querySelectorAll('.bullet-icons-item');

const swiperSlideContainer = document.querySelectorAll('.swiper-slide');

bulletIconItems[0].classList.add('swiper-icon-active');

bulletIconContainer.addEventListener('click', swiperContentDisplay);

const swiperSlideFirstItem = document.querySelector('.swiper-slide');
const bulletIconItem = document.querySelector('.bullet-icons-item');

if (bulletIconItem.dataset.article === swiperSlideFirstItem.dataset.article) {
  swiperSlideFirstItem.classList.add('swiper-slide-active');
}

function swiperContentDisplay(e) {
  const selectedPicture = e.target.closest('img');
  if (!selectedPicture) {
    return;
  }
  const activeSwiperSlide = document.querySelector('.swiper-icon-active');

  if (activeSwiperSlide) {
    activeSwiperSlide.classList.remove('swiper-icon-active');
  }
  selectedPicture.classList.add('swiper-icon-active');

  swiperSlideContainer.forEach((item) => {
    item.classList.remove('swiper-slide-active');
    if (selectedPicture.dataset.article === item.dataset.article) {
      item.classList.add('swiper-slide-active');
    }
  });
}
const dynamicArrowsContainer = document.querySelector('.dynamic-items-wrapper');

const rightArrow = document.querySelector('.swiper-button-right');
const leftArrow = document.querySelector('.swiper-button-left');

dynamicArrowsContainer.addEventListener('click', dynamicArrows);

function dynamicArrows(e) {
  const arrow = e.target.closest('div');

  for (let index = 0; index <= bulletIconItems.length; index++) {
    const elem = bulletIconItems[index];

    if (arrow.contains(leftArrow) && elem.classList.contains('swiper-icon-active')) {
      if (index === 0) {
        index = bulletIconItems.length;
      }
      elem.classList.remove('swiper-icon-active');
      bulletIconItems[index - 1].classList.add('swiper-icon-active');
      break;
    }

    if (bulletIconItems.length - 1 === index) {
      index = -1;
    }
    if (arrow.contains(rightArrow) && elem.classList.contains('swiper-icon-active')) {
      elem.classList.remove('swiper-icon-active');
      bulletIconItems[index + 1].classList.add('swiper-icon-active');
      break;
    }
  }

  swiperSlideContainer.forEach((item) => {
    item.classList.remove('swiper-slide-active');
    bulletIconItems.forEach((elem) => {
      if (elem.classList.contains('swiper-icon-active') && elem.dataset.article === item.dataset.article) {
        item.classList.add('swiper-slide-active');
      }
    });
  });
}
