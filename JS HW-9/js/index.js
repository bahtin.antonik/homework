const arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
const arr2 = ['1', '2', '3', 'sea', 'user', 23];

const divEl = document.querySelector('.list');

function attachToParrent(arr, attachList = document.body) {
  const ol = document.createElement('ol');
  arr.forEach((element) => {
    const el = document.createElement('li');
    el.innerText = element;
    ol.append(el);
  });
  attachList.append(ol);
}

attachToParrent(arr, divEl);
attachToParrent(arr2);

/*Теоретичні питання
Опишіть, як можна створити новий HTML тег на сторінці.
Новий тег можна створити через createElement та написати назву тегу.
Також через insertAdjacentHTML та InnerHTML

Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
Перший параметр position задає місце розміщення самого тексту insertAdjacentHTML.
Можливі варіації параметра afterbegin	тобто після початку firstchild
afterend	після елементу
beforebegin	перед елементом
beforeend	в самому кінці тобто lastchild

Як можна видалити елемент зі сторінки?
Видалити елемент зі сторінки можна за допомогою element.remove();
*/
