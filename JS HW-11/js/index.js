const eyePassCollection = document.querySelectorAll('.icon-password');
const inputCollection = document.querySelectorAll('input');
const wrapperCollection = document.querySelectorAll('.input-wrapper');
const submitBtn = document.querySelector('.btn');

eyePassCollection.forEach((eye) => {
  eye.addEventListener('click', (e) => {
    const input = e.currentTarget.previousSibling.previousSibling;

    if (input.type === 'password') {
      input.type = 'text';
      eye.classList.replace('fa-eye', 'fa-eye-slash');
    } else {
      input.type = 'password';
      eye.classList.replace('fa-eye-slash', 'fa-eye');
    }
  });
});

submitBtn.addEventListener('click', (e) => {
  e.preventDefault();
  if (inputCollection[0].value === inputCollection[1].value) {
    alert('You are welcome');
  } else {
    alert('Потрібно ввести однакові значення');
  }
});
