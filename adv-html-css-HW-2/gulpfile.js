import gulp from 'gulp';
import { path } from './gulp/config/path.js';
import { plugins } from './gulp/config/plugins.js';

global.app = {
  isBuild: process.argv.includes('--build'),
  isDev: !process.argv.includes('--build'),
  gulp,
  path,
  plugins,
};

import { reset } from './gulp/tasks/reset.js';
import { html } from './gulp/tasks/html.js';
import { img } from './gulp/tasks/img.js';
import { server } from './gulp/tasks/server.js';
import { scss } from './gulp/tasks/scss.js';
import { js } from './gulp/tasks/js.js';

const watcher = () => {
  gulp.watch(path.watch.html, html);
  gulp.watch(path.watch.img, img);
  gulp.watch(path.watch.scss, scss);
  gulp.watch(path.watch.js, js);
};

const mainTasks = gulp.parallel(html, img, scss, js);

export const build = gulp.series(reset, mainTasks);

export const dev = gulp.series(reset, mainTasks, gulp.parallel(watcher, server));
