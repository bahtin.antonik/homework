const burgerMenu = document.querySelector('.burger-menu');
const burgerMenuToggle = document.querySelector('.burger-menu-container');
const openBurgerIcon = document.querySelector('.burger-opened-tab');
const closeBurgerIcon = document.querySelector('.burger-unopened-tab');

burgerMenu.addEventListener('click', (e) => {
  if (e.currentTarget.classList.contains('active')) {
    burgerMenu.classList.remove('active');
  } else {
    burgerMenu.classList.add('active');
  }
});
