// task 1
const paragraphContainer = document.querySelectorAll('p');
paragraphContainer.forEach((p) => (p.style.background = '#ff0000'));

// task 2
const optionListEl = document.getElementById('optionsList');
console.log(optionListEl);

console.log(optionListEl.parentElement);

optionListEl.childNodes.forEach((e) => console.log(e.nodeType));
optionListEl.childNodes.forEach((e) => console.log(e.nodeName));

// task 3
const testParagraph = document.querySelector('#testParagraph');
testParagraph.textContent = 'This is a paragraph';

// task 4-5
const mainHeader = document.querySelector('.main-header');
console.log(mainHeader.childNodes);

const mainHeaderChildren = mainHeader.childNodes;
mainHeaderChildren.forEach((element) => element.classList?.add('nav-item'));

//task 6
const sectionTitleContainer = document.querySelectorAll('.section-title');
sectionTitleContainer.forEach((element) => element.classList?.remove('section-title'));
console.log(sectionTitleContainer);

/* Теоретичні питання
Опишіть своїми словами що таке Document Object Model (DOM)
DOM - це модель яка відображає нашу структуру на сторінці сайту, DOM зображає документ як 
ноди та об'єкти.

Яка різниця між властивостями HTML-елементів innerHTML та innerText?
innerHTML виводить html елементів разом із текстом
innerText виводіть тільки текст елементу

Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
До елемента сторінки можна звернутися за допомогою тега, класу та айді.
Найкраще використовувати querySelector та querySelectorAll. Це потужні нові інструменти
які заміняють усі старі способи виведення.
 */
