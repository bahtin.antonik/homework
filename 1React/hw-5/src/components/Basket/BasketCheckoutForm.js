import React from 'react';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import { CLEAR_BASKET_ITEMS } from '../../store/actions';

const BasketCheckoutForm = () => {
  const dispatch = useDispatch();
  const basketItems = useSelector((state) => state.basket.basketItems);

  return (
    <Formik
      initialValues={{
        firstName: '',
        lastName: '',
        userAge: '',
        deliveryAddress: '',
        phoneNumber: '+380',
      }}
      validationSchema={Yup.object({
        firstName: Yup.string().max(20, 'Must be 20 characters or less').required('Required'),
        lastName: Yup.string().max(20, 'Must be 20 characters or less').required('Required'),
        userAge: Yup.number()
          .typeError('Please enter a number')
          .max(120, 'Must be 3 characters or less')
          .required('Required'),
        deliveryAddress: Yup.string().max(100, 'Invalid delivery address').required('Required'),
        phoneNumber: Yup.string().length(13, 'Invalid phone number').required('Required'),
      })}
      onSubmit={(values) => {
        console.log(basketItems);
        console.log(JSON.stringify(values, null, 2));
        dispatch({ type: CLEAR_BASKET_ITEMS });
      }}
    >
      {({ handleChange, values, touched, errors, handleBlur }) => (
        <Form>
          <label htmlFor="firstName">First Name</label>
          <input
            id="firstName"
            name="firstName"
            type="text"
            onChange={handleChange}
            value={values.firstName}
            onBlur={handleBlur}
            className={touched.firstName && errors.firstName ? 'input-error' : ''}
          />
          {touched.firstName && errors.firstName && <div>{errors.firstName}</div>}
          <label htmlFor="lastName">Last Name</label>
          <input
            id="lastName"
            name="lastName"
            type="text"
            onChange={handleChange}
            value={values.lastName}
            onBlur={handleBlur}
            className={touched.lastName && errors.lastName ? 'input-error' : ''}
          />
          {touched.lastName && errors.lastName && <div>{errors.lastName}</div>}
          <label htmlFor="userAge">Age</label>
          <input
            id="userAge"
            name="userAge"
            type="text"
            onChange={handleChange}
            value={values.userAge}
            onBlur={handleBlur}
            className={touched.userAge && errors.userAge ? 'input-error' : ''}
          />
          {touched.userAge && errors.userAge && <div>{errors.userAge}</div>}
          <label htmlFor="deliveryAddress">Delivery Address</label>
          <input
            id="deliveryAddress"
            name="deliveryAddress"
            type="text"
            onChange={handleChange}
            value={values.deliveryAddress}
            onBlur={handleBlur}
            className={touched.deliveryAddress && errors.deliveryAddress ? 'input-error' : ''}
          />
          {touched.deliveryAddress && errors.deliveryAddress && <div>{errors.deliveryAddress}</div>}
          <label htmlFor="phoneNumber">Phone Number</label>
          <input
            id="phoneNumber"
            name="phoneNumber"
            type="text"
            onChange={handleChange}
            value={values.phoneNumber}
            onBlur={handleBlur}
            className={touched.phoneNumber && errors.phoneNumber ? 'input-error' : ''}
          />
          {touched.phoneNumber && errors.phoneNumber && <div>{errors.phoneNumber}</div>}
          <button
            className="checkout-button"
            type="submit"
          >
            Checkout
          </button>
        </Form>
      )}
    </Formik>
  );
};

export default BasketCheckoutForm;
