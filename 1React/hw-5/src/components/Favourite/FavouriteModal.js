import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CLOSED_FAVOURITE_MODAL } from '../../store/actions';
import { addProductToFavourite, deleteProductFromFavourite } from '../../store/modal/modalThunk';
import Button from '../Button';
import Modal from '../Modal';

const FavouriteModal = () => {
  const isModalOpen = useSelector((state) => state.modal.isFavouriteModalOpen);

  const isItemInFavourite = useSelector((state) => state.modal.isItemInFavourite);

  const dispatch = useDispatch();

  const addInFavourite = () => {
    dispatch(addProductToFavourite());
  };

  const closeModal = () => {
    dispatch({ type: CLOSED_FAVOURITE_MODAL });
  };

  const deleteItemFromFavourite = () => {
    dispatch(deleteProductFromFavourite());
  };

  if (!isModalOpen) {
    return null;
  }
  return (
    <Modal
      header={isItemInFavourite ? 'Видалити з обраного?' : 'Додати у обране?'}
      closeButton={true}
      text={
        isItemInFavourite
          ? 'Ви точно хочете видалити цей товар з обраного?'
          : 'Ви точно хочете додати цей товар у обране?'
      }
      onClose={closeModal}
      actions={
        <>
          <Button
            className="Button button-large"
            text="Ok"
            onClick={isItemInFavourite ? deleteItemFromFavourite : addInFavourite}
            backgroundColor="#4a7c4a"
          ></Button>
          <Button className="Button button-large" text="Cancel" onClick={closeModal} backgroundColor="#893c3c"></Button>
        </>
      }
    />
  );
};

export default FavouriteModal;
