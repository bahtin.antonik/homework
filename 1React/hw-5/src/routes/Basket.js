import React from 'react';
import ProductCard from '../components/ProductCard';
import { useSelector } from 'react-redux';
import BasketCheckoutForm from '../components/Basket/BasketCheckoutForm';

const Basket = () => {
  const basketItems = useSelector((state) => state.basket.basketItems);

  let basketContent = <p>No items in basket</p>;

  if (basketItems.length > 0) {
    basketContent = basketItems.map((good) => (
      <ProductCard
        key={good.article}
        name={good.name}
        price={good.price}
        image={good.image}
        color={good.color}
        article={good.article}
      />
    ));
  }
  return (
    <>
      <h2>Your Basket</h2>
      <div className="basket-container">
        <div className="goods-basket-container">{basketContent}</div>
        <div className="basket-form">{basketItems.length > 0 && <BasketCheckoutForm />}</div>
      </div>
    </>
  );
};

export default Basket;
