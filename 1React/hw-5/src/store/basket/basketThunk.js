import { REPLACE_BASKET_ITEMS } from '../actions';

export const fetchBasketItems = () => (dispatch) => {
  const basketLocal = localStorage.getItem('basket');
  if (basketLocal) {
    const parsedBasket = JSON.parse(basketLocal);
    dispatch({ type: REPLACE_BASKET_ITEMS, payload: parsedBasket });
  }
};
