import { DELETE_FROM_BASKET, ADD_TO_BASKET, REPLACE_BASKET_ITEMS, CLEAR_BASKET_ITEMS } from '../actions';

const initialState = {
  basketItems: [],
  basketLength: 0,
};

const basketReducer = (state = initialState, action) => {
  switch (action.type) {
    case DELETE_FROM_BASKET: {
      const newItems = state.basketItems.filter((item) => item.article !== action.payload);
      const basketLength = newItems.length;
      localStorage.setItem('basket', JSON.stringify(newItems));

      return { ...state, basketItems: newItems, basketLength };
    }
    case ADD_TO_BASKET: {
      const updatedState = { ...state };
      const newItem = action.payload;
      const existingItem = state.basketItems.find((item) => item.article === newItem.article);
      if (existingItem) {
        return updatedState;
      }
      updatedState.basketItems.push(newItem);
      updatedState.basketLength = updatedState.basketItems.length;
      localStorage.setItem('basket', JSON.stringify(updatedState.basketItems));
      return updatedState;
    }
    case REPLACE_BASKET_ITEMS: {
      const basketItems = action.payload;
      const basketLength = basketItems.length;
      return { ...state, basketItems, basketLength };
    }
    case CLEAR_BASKET_ITEMS: {
      const basketItems = [];
      const basketLength = basketItems.length;
      localStorage.removeItem('basket');
      return { ...state, basketItems, basketLength };
    }
    default:
      return state;
  }
};

export default basketReducer;
