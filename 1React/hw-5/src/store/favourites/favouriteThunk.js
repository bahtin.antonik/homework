import { REPLACE_FAVOURITE_ITEMS } from '../actions';

export const fetchFavouriteItems = () => (dispatch) => {
  const favouriteLocal = localStorage.getItem('favourites');
  if (favouriteLocal) {
    const parsedFavourite = JSON.parse(favouriteLocal);
    dispatch({ type: REPLACE_FAVOURITE_ITEMS, payload: parsedFavourite });
  }
};
