import { ADD_TO_FAVOURITE, DELETE_FROM_FAVOURITE, REPLACE_FAVOURITE_ITEMS } from '../actions';

const initialState = {
  favouriteItems: [],
  favouriteLength: 0,
};

const favouriteReducer = (state = initialState, action) => {
  switch (action.type) {
    case DELETE_FROM_FAVOURITE: {
      const newItems = state.favouriteItems.filter((item) => item.article !== action.payload);
      const favouriteLength = newItems.length;
      localStorage.setItem('favourites', JSON.stringify(newItems));

      return { ...state, favouriteItems: newItems, favouriteLength };
    }
    case ADD_TO_FAVOURITE: {
      const updatedState = { ...state };
      const newItem = action.payload;
      const existingItem = state.favouriteItems.find((item) => item.article === newItem.article);
      if (existingItem) {
        return updatedState;
      }
      updatedState.favouriteItems.push(newItem);
      updatedState.favouriteLength = updatedState.favouriteItems.length;
      localStorage.setItem('favourites', JSON.stringify(updatedState.favouriteItems));
      return updatedState;
    }
    case REPLACE_FAVOURITE_ITEMS: {
      const favouriteItems = action.payload;
      const favouriteLength = favouriteItems.length;
      return { ...state, favouriteItems, favouriteLength };
    }
    default:
      return state;
  }
};
export default favouriteReducer;
