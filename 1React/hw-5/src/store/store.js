import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import { FETCH_DATA_FROM_SERVER, FETCH_DATA_FROM_SERVER_ERROR } from './actions';
import thunk from 'redux-thunk';

const composedEnhancer = composeWithDevTools(applyMiddleware(thunk));

export const store = createStore(rootReducer, composedEnhancer);

const initialState = {
  data: null,
};

const dataReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DATA_FROM_SERVER:
      return { ...state, data: action.payload };
    case FETCH_DATA_FROM_SERVER_ERROR:
      return { ...state, data: null };
    default:
      return state;
  }
};

export default store;
