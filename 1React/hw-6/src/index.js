import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import store from './store/store';
import { Provider } from 'react-redux';
import SwitchContextProvider from './context/switchContext';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <SwitchContextProvider>
        <App />
      </SwitchContextProvider>
    </Provider>
  </React.StrictMode>
);
reportWebVitals();
