import { HiOutlineSquares2X2 } from 'react-icons/hi2';
import { AiOutlineBars } from 'react-icons/ai';

import { useContext } from 'react';
import { SwitchContext } from '../context/switchContext';

const Switch = () => {
  const switchValue = useContext(SwitchContext);
  console.log(switchValue);

  function setGrid() {
    switchValue.setSwitchMode('grid');
  }

  function setList() {
    switchValue.setSwitchMode('list');
  }

  return (
    <div className="switch-wrapper">
      <button
        onClick={setList}
        disabled={switchValue.switchMode === 'list'}
      >
        <AiOutlineBars size={30} />
      </button>
      <button
        onClick={setGrid}
        disabled={switchValue.switchMode === 'grid'}
      >
        <HiOutlineSquares2X2 size={30} />
      </button>
    </div>
  );
};

export default Switch;
