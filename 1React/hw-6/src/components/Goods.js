import React, { useContext, useEffect, useState } from 'react';
import getGoods from '../api/getGoods';
import { SwitchContext } from '../context/switchContext';
import ProductCard from './ProductCard';
import Switch from './Switch';

export default function Goods() {
  const [goods, setGoods] = useState([]);

  const switchValue = useContext(SwitchContext);

  const mode = switchValue.switchMode;

  useEffect(() => {
    getGoods().then(setGoods);
  }, []);

  return (
    <>
      <Switch />
      <div className={`goods-container ${mode === 'list' ? 'goods-list' : ''}`}>
        {goods.map((good) => (
          <ProductCard
            key={good.article}
            name={good.name}
            price={good.price}
            image={good.image}
            color={good.color}
            article={good.article}
          />
        ))}
      </div>
    </>
  );
}
