import { render, fireEvent } from '@testing-library/react';
import Modal from './Modal';

describe('Modal component', () => {
  test('renders with the correct header text', () => {
    const headerText = 'Test Header';
    const { getByText } = render(<Modal header={headerText} />);
    expect(getByText(headerText)).toBeInTheDocument();
  });

  test('does not render close button when closeButton prop is false', () => {
    const { queryByRole } = render(<Modal closeButton={false} />);
    expect(queryByRole('button', { name: 'Close' })).toBeNull();
  });

  test('calls the onClose handler when the modal is clicked', () => {
    const handleClose = jest.fn();
    const { getByRole } = render(<Modal onClose={handleClose} />);
    fireEvent.click(getByRole('dialog'));
    expect(handleClose).toHaveBeenCalled();
  });
});

describe('Modal shapshot test', () => {
  test('renders component that matches snapshot', () => {
    const { asFragment } = render(
      <div
        className="Modal-wrapper"
        role="dialog"
      >
        <div
          className="Modal"
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <div className="Modal-header">
            <h2>Text</h2>
            <button className="Modal-header-close-btn">X</button>
          </div>
          <p className="Modal-text">text</p>
          <div className="Modal-submit-btns">actions</div>
        </div>
      </div>
    );
    expect(asFragment()).toMatchSnapshot();
  });
});
