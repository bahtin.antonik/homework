import { createPortal } from 'react-dom';
import PropTypes from 'prop-types';

export default function Modal({ header, closeButton, text, actions, backgroundColor, onClose }) {
  return createPortal(
    <div
      className="Modal-wrapper"
      onClick={onClose}
      role="dialog"
    >
      <div
        className="Modal"
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <div className="Modal-header">
          <h2>{header}</h2>
          {closeButton && (
            <button
              onClick={onClose}
              className="Modal-header-close-btn"
            >
              X
            </button>
          )}
        </div>
        <p className="Modal-text">{text}</p>
        <div className="Modal-submit-btns">{actions}</div>
      </div>
    </div>,
    document.body
  );
}
Modal.defaultProps = {
  className: 'Modal',
};

Modal.propTypes = {
  backgroundColor: PropTypes.string,
  onClick: PropTypes.func,
  header: PropTypes.string,
  text: PropTypes.string,
  actions: PropTypes.object,
};
