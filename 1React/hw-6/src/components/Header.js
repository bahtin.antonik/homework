import React from 'react';
import { ReactComponent as BasketIcon } from '../images/basket.svg';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

const Header = () => {
  const navigate = useNavigate();

  const basketLength = useSelector((state) => state.basket.basketLength);
  const favouriteLength = useSelector((state) => state.favourite.favouriteLength);

  return (
    <>
      <div className="Header container">
        <h1
          onClick={() => {
            navigate('/');
          }}
        >
          Light Noir
        </h1>
        <div className="header-icons">
          <div
            onClick={() => {
              navigate('favourite');
            }}
            className="header-icons-favourite"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="red"
              height="32"
              width="32"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
              />
            </svg>
            <span>{favouriteLength}</span>
          </div>
          <div
            onClick={() => {
              navigate('basket');
            }}
            className="header-icons-basket"
          >
            <BasketIcon />
            <span>{basketLength}</span>
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;
