import { render, fireEvent } from '@testing-library/react';
import Button from './Button';

describe('Button component', () => {
  test('renders with the correct text', () => {
    const buttonText = 'Click me!';
    const { getByText } = render(
      <Button
        backgroundColor="black"
        text={buttonText}
      />
    );
    expect(getByText(buttonText)).toBeInTheDocument();
  });

  test('renders with the correct background color', () => {
    const backgroundColor = 'red';
    const { getByRole } = render(
      <Button
        text="test"
        backgroundColor={backgroundColor}
      />
    );
    expect(getByRole('button')).toHaveStyle(`background-color: ${backgroundColor}`);
  });

  test('calls the onClick handler when clicked', () => {
    const handleClick = jest.fn();
    const { getByRole } = render(
      <Button
        text="test"
        backgroundColor="black"
        onClick={handleClick}
      />
    );
    fireEvent.click(getByRole('button'));
    expect(handleClick).toHaveBeenCalled();
  });
});

describe('Button shapshot test', () => {
  test('renders component that matches snapshot', () => {
    const { asFragment } = render(
      <Button
        backgroundColor="black"
        text="buttonText"
      />
    );
    expect(asFragment()).toMatchSnapshot();
  });
});
