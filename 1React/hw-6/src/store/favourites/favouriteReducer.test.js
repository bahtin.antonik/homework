import favouriteReducer from './favouriteReducer';
import { ADD_TO_FAVOURITE, DELETE_FROM_FAVOURITE, REPLACE_FAVOURITE_ITEMS } from '../actions';

describe('favouriteReducer', () => {
  it('should add item to favourite list', () => {
    const state = { favouriteItems: [], favouriteLength: 0 };
    const action = { type: ADD_TO_FAVOURITE, payload: { article: '123', name: 'Product' } };
    const expectedState = {
      favouriteItems: [{ article: '123', name: 'Product' }],
      favouriteLength: 1,
    };
    expect(favouriteReducer(state, action)).toEqual(expectedState);
  });

  it('should not add item to favourite list if it already exists', () => {
    const state = {
      favouriteItems: [{ article: '123', name: 'Product' }],
      favouriteLength: 1,
    };
    const action = { type: ADD_TO_FAVOURITE, payload: { article: '123', name: 'Product' } };
    expect(favouriteReducer(state, action)).toEqual(state);
  });

  it('should delete item from favourite list', () => {
    const state = {
      favouriteItems: [
        { article: '123', name: 'Product1' },
        { article: '456', name: 'Product2' },
      ],
      favouriteLength: 2,
    };
    const action = { type: DELETE_FROM_FAVOURITE, payload: '123' };
    const expectedState = {
      favouriteItems: [{ article: '456', name: 'Product2' }],
      favouriteLength: 1,
    };
    expect(favouriteReducer(state, action)).toEqual(expectedState);
  });

  it('should replace favourite items with new items', () => {
    const state = {
      favouriteItems: [
        { article: '123', name: 'Product1' },
        { article: '456', name: 'Product2' },
      ],
      favouriteLength: 2,
    };
    const action = {
      type: REPLACE_FAVOURITE_ITEMS,
      payload: [{ article: '789', name: 'Product3' }],
    };
    const expectedState = {
      favouriteItems: [{ article: '789', name: 'Product3' }],
      favouriteLength: 1,
    };
    expect(favouriteReducer(state, action)).toEqual(expectedState);
  });

  it('should return the initial state if action type is unknown', () => {
    const state = { favouriteItems: [], favouriteLength: 0 };
    const action = { type: 'UNKNOWN_ACTION_TYPE' };
    expect(favouriteReducer(state, action)).toEqual(state);
  });
});
