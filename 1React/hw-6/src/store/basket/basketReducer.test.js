import basketReducer from './basketReducer';
import { ADD_TO_BASKET, DELETE_FROM_BASKET, REPLACE_BASKET_ITEMS, CLEAR_BASKET_ITEMS } from '../actions';

describe('basketReducer', () => {
  it('should return the initial state', () => {
    expect(basketReducer(undefined, {})).toEqual({
      basketItems: [],
      basketLength: 0,
    });
  });

  it('should handle ADD_TO_BASKET', () => {
    const item = { article: '123', name: 'Product 1' };
    const initialState = { basketItems: [], basketLength: 0 };
    const expectedState = {
      basketItems: [item],
      basketLength: 1,
    };
    expect(basketReducer(initialState, { type: ADD_TO_BASKET, payload: item })).toEqual(expectedState);
  });

  it('should handle ADD_TO_BASKET with existing item', () => {
    const item = { article: '123', name: 'Product 1' };
    const initialState = { basketItems: [item], basketLength: 1 };
    expect(basketReducer(initialState, { type: ADD_TO_BASKET, payload: item })).toEqual(initialState);
  });

  it('should handle DELETE_FROM_BASKET', () => {
    const item1 = { article: '123', name: 'Product 1' };
    const item2 = { article: '456', name: 'Product 2' };
    const initialState = { basketItems: [item1, item2], basketLength: 2 };
    const expectedState = { basketItems: [item2], basketLength: 1 };
    expect(basketReducer(initialState, { type: DELETE_FROM_BASKET, payload: item1.article })).toEqual(expectedState);
  });

  it('should handle REPLACE_BASKET_ITEMS', () => {
    const items = [
      { article: '123', name: 'Product 1' },
      { article: '456', name: 'Product 2' },
    ];
    const initialState = { basketItems: [], basketLength: 0 };
    const expectedState = { basketItems: items, basketLength: 2 };
    expect(basketReducer(initialState, { type: REPLACE_BASKET_ITEMS, payload: items })).toEqual(expectedState);
  });

  it('should handle CLEAR_BASKET_ITEMS', () => {
    const initialState = { basketItems: [{ article: '123', name: 'Product 1' }], basketLength: 1 };
    const expectedState = { basketItems: [], basketLength: 0 };
    expect(basketReducer(initialState, { type: CLEAR_BASKET_ITEMS })).toEqual(expectedState);
  });
});
