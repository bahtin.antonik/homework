import { combineReducers } from 'redux';
import basketReducer from './basket/basketReducer';
import favouriteReducer from './favourites/favouriteReducer';
import modalReducer from './modal/modalReducer';

const rootReducer = combineReducers({
  favourite: favouriteReducer,
  basket: basketReducer,
  modal: modalReducer,
});

export default rootReducer;
