import modalReducer from './modalReducer';
import { CLOSED_BASKET_MODAL, CLOSED_FAVOURITE_MODAL, OPENED_BASKET_MODAL, OPENED_FAVOURITE_MODAL } from '../actions';

describe('modalReducer', () => {
  it('should return the initial state', () => {
    expect(modalReducer(undefined, {})).toEqual({
      isBasketModalOpen: false,
      isFavouriteModalOpen: false,
      basketItem: null,
      favouriteItem: null,
      isItemInFavourite: false,
      isItemInBasket: false,
    });
  });

  it('should handle CLOSED_BASKET_MODAL', () => {
    const initialState = {
      isBasketModalOpen: true,
      isFavouriteModalOpen: false,
      basketItem: { name: 'basket item' },
      favouriteItem: null,
      isItemInFavourite: false,
      isItemInBasket: false,
    };
    const action = {
      type: CLOSED_BASKET_MODAL,
    };
    expect(modalReducer(initialState, action)).toEqual({
      isBasketModalOpen: false,
      isFavouriteModalOpen: false,
      basketItem: null,
      favouriteItem: null,
      isItemInFavourite: false,
      isItemInBasket: false,
    });
  });

  it('should handle OPENED_BASKET_MODAL', () => {
    const initialState = {
      isBasketModalOpen: false,
      isFavouriteModalOpen: false,
      basketItem: null,
      favouriteItem: null,
      isItemInFavourite: false,
      isItemInBasket: false,
    };
    const action = {
      type: OPENED_BASKET_MODAL,
      payload: { name: 'basket item' },
      isItemInBasket: true,
    };
    expect(modalReducer(initialState, action)).toEqual({
      isBasketModalOpen: true,
      isFavouriteModalOpen: false,
      basketItem: { name: 'basket item' },
      favouriteItem: null,
      isItemInFavourite: false,
      isItemInBasket: true,
    });
  });

  it('should handle CLOSED_FAVOURITE_MODAL', () => {
    const initialState = {
      isBasketModalOpen: false,
      isFavouriteModalOpen: true,
      basketItem: null,
      favouriteItem: { name: 'favourite item' },
      isItemInFavourite: false,
      isItemInBasket: false,
    };
    const action = {
      type: CLOSED_FAVOURITE_MODAL,
    };
    expect(modalReducer(initialState, action)).toEqual({
      isBasketModalOpen: false,
      isFavouriteModalOpen: false,
      basketItem: null,
      favouriteItem: null,
      isItemInFavourite: false,
      isItemInBasket: false,
    });
  });
});
