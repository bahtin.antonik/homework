import {
  ADD_TO_BASKET,
  ADD_TO_FAVOURITE,
  CLOSED_BASKET_MODAL,
  CLOSED_FAVOURITE_MODAL,
  DELETE_FROM_BASKET,
  DELETE_FROM_FAVOURITE,
} from '../actions';

export function addProductToBasket() {
  return (dispatch, getState) => {
    const state = getState();
    const product = state.modal.basketItem;
    dispatch({ type: ADD_TO_BASKET, payload: product });
    dispatch({ type: CLOSED_BASKET_MODAL });
  };
}

export function deleteProductFromBasket() {
  return (dispatch, getState) => {
    const state = getState();
    const product = state.modal.basketItem;
    dispatch({ type: DELETE_FROM_BASKET, payload: product.article });
    dispatch({ type: CLOSED_BASKET_MODAL });
  };
}

export function addProductToFavourite() {
  return (dispatch, getState) => {
    const state = getState();
    const product = state.modal.favouriteItem;
    dispatch({ type: ADD_TO_FAVOURITE, payload: product });
    dispatch({ type: CLOSED_FAVOURITE_MODAL });
  };
}

export function deleteProductFromFavourite() {
  return (dispatch, getState) => {
    const state = getState();
    const product = state.modal.favouriteItem;
    dispatch({ type: DELETE_FROM_FAVOURITE, payload: product.article });
    dispatch({ type: CLOSED_FAVOURITE_MODAL });
  };
}
