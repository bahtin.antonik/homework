import { CLOSED_BASKET_MODAL, CLOSED_FAVOURITE_MODAL, OPENED_BASKET_MODAL, OPENED_FAVOURITE_MODAL } from '../actions';

const initialState = {
  isBasketModalOpen: false,
  isFavouriteModalOpen: false,
  basketItem: null,
  favouriteItem: null,
  isItemInFavourite: false,
  isItemInBasket: false,
};

const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case CLOSED_BASKET_MODAL: {
      return { ...state, isBasketModalOpen: false, basketItem: null };
    }
    case OPENED_BASKET_MODAL: {
      return { ...state, isBasketModalOpen: true, basketItem: action.payload, isItemInBasket: action.isItemInBasket };
    }
    case CLOSED_FAVOURITE_MODAL: {
      return { ...state, isFavouriteModalOpen: false, favouriteItem: null };
    }
    case OPENED_FAVOURITE_MODAL: {
      return {
        ...state,
        isFavouriteModalOpen: true,
        favouriteItem: action.payload,
        isItemInFavourite: action.isItemInFavourite,
      };
    }
    default:
      return state;
  }
};

export default modalReducer;
