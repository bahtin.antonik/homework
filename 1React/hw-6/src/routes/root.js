import React from 'react';
import { Outlet } from 'react-router-dom';
import Header from '../components/Header';
import BasketModal from '../components/Basket/BasketModal';
import FavouriteModal from '../components/Favourite/FavouriteModal';

const Root = () => {
  return (
    <>
      <Header />
      <div className="container">
        <Outlet />
      </div>
      <BasketModal />
      <FavouriteModal />
    </>
  );
};

export default Root;
