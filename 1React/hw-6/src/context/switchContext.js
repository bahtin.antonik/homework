import { createContext, useState } from 'react';

export const SwitchContext = createContext({ switchMode: 'grid', setSwitchMode: () => {} });
function SwitchContextProvider({ children }) {
  const [switchMode, setSwitchMode] = useState('grid');
  return <SwitchContext.Provider value={{ switchMode, setSwitchMode }}>{children}</SwitchContext.Provider>;
}

export default SwitchContextProvider;
