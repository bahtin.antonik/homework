import React from 'react';
import ProductCard from './ProductCard';
import { useSelector } from 'react-redux';

const BasketComponent = () => {
  const basketItems = useSelector((state) => state.basket.basketItems);

  let basketContent = <p>No items in basket</p>;

  if (basketItems.length > 0) {
    basketContent = basketItems.map((good) => (
      <ProductCard
        key={good.article}
        name={good.name}
        price={good.price}
        image={good.image}
        color={good.color}
        article={good.article}
      />
    ));
  }
  return (
    <>
      <h2>Your Basket</h2>
      <div className="Goods-container">{basketContent}</div>
    </>
  );
};

export default BasketComponent;
