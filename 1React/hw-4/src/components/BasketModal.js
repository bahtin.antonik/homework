import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CLOSED_BASKET_MODAL } from '../store/actions';
import { addProductToBasket, deleteProductFromBasket } from '../store/modal/modalThunk';
import Button from './Button';
import Modal from './Modal';

const BasketModal = () => {
  const isModalOpen = useSelector((state) => state.modal.isBasketModalOpen);

  const isItemInBasket = useSelector((state) => state.modal.isItemInBasket);

  const dispatch = useDispatch();

  const addInBasket = () => {
    dispatch(addProductToBasket());
  };

  const closeModal = () => {
    dispatch({ type: CLOSED_BASKET_MODAL });
  };

  const deleteItemFromBasket = () => {
    dispatch(deleteProductFromBasket());
  };

  if (!isModalOpen) {
    return null;
  }

  return (
    <Modal
      header={isItemInBasket ? 'Видалити із кошика?' : 'Додати у кошик?'}
      closeButton={true}
      text={
        isItemInBasket ? 'Ви точно хочете видалити цей товар із кошика?' : 'Ви точно хочете додати цей товар у кошик?'
      }
      onClose={closeModal}
      actions={
        <>
          <Button
            className="Button"
            text="Ok"
            onClick={isItemInBasket ? deleteItemFromBasket : addInBasket}
            backgroundColor="#4a7c4a"
          ></Button>
          <Button className="Button" text="Cancel" onClick={closeModal} backgroundColor="#893c3c"></Button>
        </>
      }
    />
  );
};

export default BasketModal;
