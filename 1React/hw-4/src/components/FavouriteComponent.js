import React from 'react';
import { useSelector } from 'react-redux';
import ProductCard from './ProductCard';

const FavouriteComponent = () => {
  const favouriteItems = useSelector((state) => state.favourite.favouriteItems);

  let favouriteContent = <p>No items in favourite</p>;

  if (favouriteItems.length > 0) {
    favouriteContent = favouriteItems.map((good) => (
      <ProductCard
        key={good.article}
        name={good.name}
        price={good.price}
        image={good.image}
        color={good.color}
        article={good.article}
      />
    ));
  }
  return (
    <>
      <h2>Your Favourite</h2>
      <div className="Goods-container">{favouriteContent}</div>
    </>
  );
};

export default FavouriteComponent;
