import React, { useEffect, useState } from 'react';
import getGoods from '../api/getGoods';
import ProductCard from './ProductCard';

export default function Goods(props) {
  const [goods, setGoods] = useState([]);

  useEffect(() => {
    getGoods().then(setGoods);
  }, []);

  return (
    <>
      <div className="Goods-container">
        {goods.map((good) => (
          <ProductCard
            key={good.article}
            name={good.name}
            price={good.price}
            image={good.image}
            color={good.color}
            article={good.article}
          />
        ))}
      </div>
    </>
  );
}
