import PropTypes from 'prop-types';
import Button from './Button';
import { useDispatch, useSelector } from 'react-redux';
import { OPENED_BASKET_MODAL, OPENED_FAVOURITE_MODAL } from '../store/actions';

export default function ProductCard({ image, name, article, price, color }) {
  const dispatch = useDispatch();

  const basketItem = useSelector((state) =>
    state.basket.basketItems.find((itemInBasket) => itemInBasket.article === article)
  );
  const favouriteItem = useSelector((state) =>
    state.favourite.favouriteItems.find((itemInFavourite) => itemInFavourite.article === article)
  );

  const isItemInBasket = !!basketItem;

  const isItemInFavourite = !!favouriteItem;

  const openBasketModal = () => {
    dispatch({ type: OPENED_BASKET_MODAL, payload: { image, name, article, price, color }, isItemInBasket });
  };

  const openFavouriteModal = () => {
    dispatch({ type: OPENED_FAVOURITE_MODAL, payload: { image, name, article, price, color }, isItemInFavourite });
  };

  const fillColor = isItemInFavourite ? 'red' : 'none';
  return (
    <div className="product-card">
      <a href="#" className="product-card-image">
        <img src={image} alt={name} />
        <span className="product-card-name">{name}</span>
        <div className="heart-icon" onClick={openFavouriteModal}>
          <svg xmlns="http://www.w3.org/2000/svg" fill={fillColor} viewBox="0 0 24 24" stroke="currentColor">
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
            />
          </svg>
        </div>
      </a>
      <span className="product-card-price">{price}</span>
      <span className="product-card-color">Колір: {color}</span>
      <Button
        className="Button"
        text={isItemInBasket ? 'Видалити із кошика' : 'Додати у кошик'}
        backgroundColor="black"
        onClick={openBasketModal}
      />
    </div>
  );
}

ProductCard.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  article: PropTypes.number.isRequired,
  price: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
};
