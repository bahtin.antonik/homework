import React from 'react';
import { Outlet } from 'react-router-dom';
import Header from '../components/Header';
import BasketModal from '../components/BasketModal';
import FavouriteModal from '../components/FavouriteModal';

const Root = () => {
  return (
    <>
      <Header />
      <div className="container">
        <Outlet />
      </div>
      <BasketModal />
      <FavouriteModal />
    </>
  );
};

export default Root;
