import './App.scss';
import React, { useEffect } from 'react';
import Goods from './components/Goods';
import { useDispatch } from 'react-redux';
import { fetchBasketItems } from './store/basket/basketThunk';

import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import ErrorPage from './error-page';
import BasketComponent from './components/BasketComponent';
import FavouriteComponent from './components/FavouriteComponent';
import Layout from './routes/root';
import { fetchFavouriteItems } from './store/favourites/favouriteThunk';

const router = createBrowserRouter([
  {
    path: '/',
    element: <Layout />,
    errorElement: <ErrorPage />,
    children: [
      {
        index: true,
        element: <Goods />,
      },
      {
        path: 'basket',
        element: <BasketComponent />,
      },
      {
        path: 'favourite',
        element: <FavouriteComponent />,
      },
    ],
  },
]);

const App = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchBasketItems());
    dispatch(fetchFavouriteItems());
  }, []);

  return <RouterProvider router={router} />;
};
export default App;
