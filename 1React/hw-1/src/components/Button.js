import React from 'react';
export class Button extends React.Component {
  render() {
    return (
      <button
        className={this.props.className}
        onClick={this.props.onClick}
        style={{ backgroundColor: this.props.backgroundColor }}
      >
        {this.props.text}
      </button>
    );
  }
}
