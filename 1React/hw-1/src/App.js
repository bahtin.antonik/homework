import './App.scss';
import React, { useState } from 'react';
import { Button } from './components/Button';
import { Modal } from './components/Modal';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isClickedFirstModal: false, isClickedSecondModal: false };
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Button
            className="Button"
            text="Open first modal"
            onClick={this.handleClickFirstModal}
            backgroundColor="#b53726"
          ></Button>
          <Button
            className="Button"
            text="Open second modal"
            onClick={this.handleClickSecondModal}
            backgroundColor="#2e652e"
          ></Button>
          {this.state.isClickedFirstModal && (
            <Modal
              header="Do you want to delete this file?"
              closeButton={false}
              text="Once you delete this file, it won’t be possible to undo this action. 
Are you sure you want to delete it?"
              onClose={this.onCloseFirstModal}
              actions={
                <>
                  <Button
                    className="Button"
                    text="Ok"
                    onClick={this.onCloseFirstModal}
                    backgroundColor="#b53726"
                  ></Button>
                  <Button
                    className="Button"
                    text="Cancel"
                    onClick={this.onCloseFirstModal}
                    backgroundColor="#b53726"
                  ></Button>
                </>
              }
            />
          )}
          {this.state.isClickedSecondModal && (
            <Modal
              header="Do you want to download this file?"
              closeButton={true}
              text="You want download this file?"
              onClose={this.onCloseSecondModal}
              actions={
                <>
                  <Button
                    className="Button"
                    text="Ok"
                    onClick={this.onCloseSecondModal}
                    backgroundColor="#30a93b"
                  ></Button>
                  <Button
                    className="Button"
                    text="Cancel"
                    onClick={this.onCloseSecondModal}
                    backgroundColor="#30a93b"
                  ></Button>
                </>
              }
            />
          )}
        </header>
      </div>
    );
  }

  handleClickFirstModal = () => {
    this.setState({ ...this.state, isClickedFirstModal: true });
  };

  handleClickSecondModal = () => {
    this.setState({ ...this.state, isClickedSecondModal: true });
  };

  onCloseFirstModal = () => {
    this.setState({ ...this.state, isClickedFirstModal: false });
  };

  onCloseSecondModal = () => {
    this.setState({ ...this.state, isClickedSecondModal: false });
  };
}
