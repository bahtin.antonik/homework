import PropTypes from 'prop-types';

function Button({ className = 'Button', backgroundColor, onClick, text }) {
  return (
    <button className={className} onClick={onClick} style={{ backgroundColor }}>
      {text}
    </button>
  );
}

Button.propTypes = {
  backgroundColor: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
};

export default Button;
