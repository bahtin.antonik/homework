import Heart from './FavouriteHeart';
import Basket from './Basket';
import PropTypes from 'prop-types';

export default function ProductCard({ image, name, article, price, color }) {
  return (
    <div className="product-card">
      <a href="#" className="product-card-image">
        <img src={image} alt={name} />
        <span className="product-card-name">{name}</span>
        <Heart article={article} className="heart-icon" />
      </a>

      <span className="product-card-price">{price}</span>
      <span className="product-card-color">Колір: {color}</span>
      <Basket article={article}></Basket>
    </div>
  );
}

ProductCard.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  article: PropTypes.number.isRequired,
  price: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
};
