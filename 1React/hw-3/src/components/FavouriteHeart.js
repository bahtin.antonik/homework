import React, { useState, useEffect, useContext } from 'react';
import Modal from './Modal';
import Button from './Button';
import FavouriteContext from '../store/favouriteContext';

const Heart = (props) => {
  const [isFavourite, setIsFavourite] = useState(false);
  const [isClickedModal, setIsClickedModal] = useState(false);
  const favouriteContext = useContext(FavouriteContext);

  useEffect(() => {
    const favourites = JSON.parse(localStorage.getItem('favourites'));
    if (favourites) {
      setIsFavourite(favourites.includes(props.article));
    }
  }, [props.article]);

  const toggleModal = (e) => {
    e.preventDefault();
    setIsClickedModal(!isClickedModal);
  };

  const addInFavourite = () => {
    favouriteContext.onAddToFavourites(props.article);
    setIsFavourite(true);
    setIsClickedModal(false);
  };

  const deleteFromFavourite = () => {
    favouriteContext.onDeleteFromFavourites(props.article);
    setIsFavourite(false);
    setIsClickedModal(false);
  };

  const onCloseModal = () => {
    setIsClickedModal(false);
  };

  const fillColor = isFavourite ? 'red' : 'none';

  return (
    <div className="heart-icon" onClick={toggleModal}>
      <svg xmlns="http://www.w3.org/2000/svg" fill={fillColor} viewBox="0 0 24 24" stroke="currentColor">
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
        />
      </svg>
      {isClickedModal && !isFavourite && (
        <Modal
          header="Додати у обране?"
          closeButton={true}
          text="Ви точно хочете додати цей товар у обране?"
          onClose={onCloseModal}
          actions={
            <>
              <Button className="Button" text="Ok" onClick={addInFavourite} backgroundColor="#4a7c4a"></Button>
              <Button className="Button" text="Cancel" onClick={onCloseModal} backgroundColor="#893c3c"></Button>
            </>
          }
        />
      )}
      {isClickedModal && isFavourite && (
        <Modal
          header="Видалити з обраного?"
          closeButton={true}
          text="Ви точно хочете видалити цей товар з обраного?"
          onClose={onCloseModal}
          actions={
            <>
              <Button className="Button" text="Ok" onClick={deleteFromFavourite} backgroundColor="#4a7c4a"></Button>
              <Button className="Button" text="Cancel" onClick={onCloseModal} backgroundColor="#893c3c"></Button>
            </>
          }
        />
      )}
    </div>
  );
};

export default Heart;
