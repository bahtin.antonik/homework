import React from 'react';
import ProductCard from './ProductCard';
import { useContext } from 'react';
import FavouriteContext from '../store/favouriteContext';

const FavouriteComponent = () => {
  const favouriteContext = useContext(FavouriteContext);

  let favouriteContent = <p>No items in favourite</p>;

  if (favouriteContext.favouriteLength > 0) {
    favouriteContent = favouriteContext.favouriteItems.map((good) => (
      <ProductCard
        key={good.article}
        name={good.name}
        price={good.price}
        image={good.image}
        color={good.color}
        article={good.article}
      />
    ));
  }
  return (
    <>
      <h2>Your Favourite</h2>
      <div className="Goods-container">{favouriteContent}</div>
    </>
  );
};

export default FavouriteComponent;
