import React from 'react';
import ProductCard from './ProductCard';
import { useContext } from 'react';
import BasketContext from '../store/basketContext';

const BasketComponent = () => {
  const basketContext = useContext(BasketContext);

  let basketContent = <p>No items in basket</p>;

  if (basketContext.basketLength > 0) {
    basketContent = basketContext.basketItems.map((good) => (
      <ProductCard
        key={good.article}
        name={good.name}
        price={good.price}
        image={good.image}
        color={good.color}
        article={good.article}
      />
    ));
  }
  return (
    <>
      <h2>Your Basket</h2>
      <div className="Goods-container">{basketContent}</div>
    </>
  );
};

export default BasketComponent;
