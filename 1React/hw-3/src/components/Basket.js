import React, { useState, useEffect, useContext } from 'react';
import Modal from './Modal';
import Button from './Button';
import BasketContext from '../store/basketContext';

const Basket = (props) => {
  const [isInBasket, setIsInBasket] = useState(false);
  const [isClickedModal, setIsClickedModal] = useState(false);
  const basketContext = useContext(BasketContext);

  const toggleModal = () => {
    setIsClickedModal(!isClickedModal);
  };

  const deleteItemFromBasket = () => {
    basketContext.onDeleteFromBasket(props.article);
    setIsClickedModal(false);
    setIsInBasket(false);
  };

  useEffect(() => {
    const itemInBasket = basketContext.basketItems.find((item) => item.article === props.article);
    if (itemInBasket) {
      setIsInBasket(true);
    }
  }, []);

  const addInBasket = () => {
    setIsInBasket(true);
    basketContext.onAddToBasket(props.article);
    setIsClickedModal(false);
  };

  const onCloseModal = () => {
    setIsClickedModal(false);
  };

  return (
    <>
      <Button
        className="Button"
        text={isInBasket ? 'Видалити із кошика' : 'Додати у кошик'}
        backgroundColor="black"
        onClick={toggleModal}
      />
      {isClickedModal && (
        <Modal
          header={isInBasket ? 'Видалити із кошика?' : 'Додати у кошик?'}
          closeButton={true}
          text={
            isInBasket ? 'Ви точно хочете видалити цей товар із кошика?' : 'Ви точно хочете додати цей товар у кошик?'
          }
          onClose={onCloseModal}
          actions={
            <>
              <Button
                className="Button"
                text="Ok"
                onClick={isInBasket ? deleteItemFromBasket : addInBasket}
                backgroundColor="#4a7c4a"
              ></Button>
              <Button className="Button" text="Cancel" onClick={onCloseModal} backgroundColor="#893c3c"></Button>
            </>
          }
        />
      )}
    </>
  );
};

export default Basket;
