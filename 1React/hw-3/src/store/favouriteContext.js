import { createContext, useEffect, useState } from 'react';
import addFavourites from '../api/addItemInFavourite';
import getFavouriteGoods from '../api/getFavouriteGoods';
import deleteFromFavourite from '../api/deleteFromFavourite';

const FavouriteContext = createContext({
  favouriteItems: {},
  favouriteLength: 0,
  onAddToFavourites: (article) => {},
  onDeleteFromFavourites: (article) => {},
});

export const FavouriteContextProvider = (props) => {
  const [favouriteItems, setFavouriteItems] = useState([]);

  const fetchOrUpdateFavorites = async () => {
    const favouriteGoods = await getFavouriteGoods();
    setFavouriteItems(favouriteGoods);
  };

  useEffect(() => {
    fetchOrUpdateFavorites();
  }, []);

  const onAddToFavourites = async (article) => {
    addFavourites(article);
    fetchOrUpdateFavorites();
  };

  const onDeleteFromFavourites = async (article) => {
    deleteFromFavourite(article);
    fetchOrUpdateFavorites();
  };

  return (
    <FavouriteContext.Provider
      value={{ favouriteItems, onDeleteFromFavourites, favouriteLength: favouriteItems.length, onAddToFavourites }}
    >
      {props.children}
    </FavouriteContext.Provider>
  );
};
export default FavouriteContext;
