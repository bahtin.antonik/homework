import { createContext, useEffect, useState } from 'react';
import addBasket from '../api/addItemInBasket';
import getBasketGoods from '../api/getBasketGoods';
import deleteFromBasket from '../api/deleteFromBasket';

const BasketContext = createContext({
  basketItems: {},
  basketLength: 0,
  onAddToBasket: (article) => {},
  onDeleteFromBasket: (article) => {},
});

export const BasketContextProvider = (props) => {
  const [basketItems, setBasketItems] = useState([]);

  const fetchOrUpdateBasket = async () => {
    const basketGoods = await getBasketGoods();
    setBasketItems(basketGoods);
  };

  useEffect(() => {
    fetchOrUpdateBasket();
  }, []);

  const onAddToBasket = async (article) => {
    addBasket(article);
    fetchOrUpdateBasket();
  };

  const onDeleteFromBasket = async (article) => {
    deleteFromBasket(article);
    fetchOrUpdateBasket();
  };

  return (
    <BasketContext.Provider
      value={{ basketItems, onDeleteFromBasket, basketLength: basketItems.length, onAddToBasket }}
    >
      {props.children}
    </BasketContext.Provider>
  );
};
export default BasketContext;
