import './App.scss';
import React, { useEffect, useState } from 'react';
import getGoods from './api/getGoods';
import Goods from './components/Goods';

const App = () => {
  return (
    <div className="App">
      <Goods></Goods>
    </div>
  );
};
export default App;
