import React from 'react';
import { Outlet } from 'react-router-dom';
import Header from '../components/Header';
import { FavouriteContextProvider } from '../store/favouriteContext';
import BasketContext, { BasketContextProvider } from '../store/basketContext';

const Root = () => {
  return (
    <>
      <BasketContextProvider>
        <FavouriteContextProvider>
          <Header />
          <div className="container">
            <Outlet />
          </div>
        </FavouriteContextProvider>
      </BasketContextProvider>
    </>
  );
};

export default Root;
