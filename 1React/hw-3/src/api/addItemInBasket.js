let basket = [];

export default function addItemInBasket(article) {
  const localBasket = JSON.parse(localStorage.getItem('basket'));

  if (localBasket) {
    basket = localBasket;
  }
  basket.push(article);
  localStorage.setItem('basket', JSON.stringify([...new Set(basket)]));
}
