import getGoods from './getGoods';

export default async function getBasketGoods() {
  const goods = await getGoods();
  const basketGoodsLocal = localStorage.getItem('basket');
  if (basketGoodsLocal) {
    const parsedBasket = JSON.parse(basketGoodsLocal);
    return goods.filter(({ article }) => parsedBasket.includes(article));
  } else {
    return [];
  }
}
