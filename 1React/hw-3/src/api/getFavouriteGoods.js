import getGoods from './getGoods';

export default async function getFavouriteGoods() {
  const goods = await getGoods();
  const favouriteGoodsLocal = localStorage.getItem('favourites');
  if (favouriteGoodsLocal) {
    const parsedFavourite = JSON.parse(favouriteGoodsLocal);
    return goods.filter(({ article }) => parsedFavourite.includes(article));
  } else {
    return [];
  }
}
