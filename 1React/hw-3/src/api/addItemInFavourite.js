let favourites = [];

export default function addFavourites(article) {
  const localFavourites = JSON.parse(localStorage.getItem('favourites'));

  if (localFavourites) {
    favourites = localFavourites;
  }
  favourites.push(article);
  localStorage.setItem('favourites', JSON.stringify([...new Set(favourites)]));
}
