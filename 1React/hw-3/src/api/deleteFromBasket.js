let basket = [];

export default function deleteFromBasket(article) {
  const localBasket = JSON.parse(localStorage.getItem('basket'));
  if (localBasket) {
    basket = localBasket;
  }
  const newBasket = basket.filter((basketItem) => {
    return basketItem !== article;
  });
  localStorage.setItem('basket', JSON.stringify(newBasket));
}
