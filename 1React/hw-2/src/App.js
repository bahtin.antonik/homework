import './App.scss';
import React from 'react';
import { getGoods } from './api/goods';
import { Header } from './components/Header';
import { Goods } from './components/Goods';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isClickedFirstModal: false, isClickedSecondModal: false };
  }
  render() {
    return (
      <div className="App">
        <Header></Header>
        <Goods goods={getGoods}></Goods>
      </div>
    );
  }
}
