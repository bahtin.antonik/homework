import React, { Component } from 'react';
import { Modal } from './Modal';
import { Button } from './Button';
import { addFavourites } from '../api/addItemInFavourite';
import { deleteFromFavourite } from '../api/deleteFromFavourite';

class Heart extends Component {
  constructor(props) {
    super(props);
    this.state = { isFavourite: false, isClickedModal: false };
    this.toggleModal = this.toggleModal.bind(this);
    this.addInFavourite = this.addInFavourite.bind(this);
    this.deleteFromFavourite = this.deleteFromFavourite.bind(this);
  }

  toggleModal(e) {
    e.preventDefault();
    this.setState({ isClickedModal: !this.state.isClickedModal });
  }

  addInFavourite() {
    this.setState({ ...this.state, isFavourite: true, isClickedModal: false });
    addFavourites(this.props.article);
  }

  deleteFromFavourite() {
    this.setState({ ...this.state, isFavourite: false, isClickedModal: false });
    deleteFromFavourite(this.props.article);
  }

  componentDidMount() {
    const favourites = JSON.parse(localStorage.getItem('favourites'));
    if (favourites) {
      const isFavourite = favourites.includes(this.props.article);
      this.setState({ ...this.state, isFavourite });
    }
  }

  onCloseModal = () => {
    this.setState({ ...this.state, isClickedModal: false });
  };

  render() {
    const fillColor = this.state.isFavourite ? 'red' : 'none';
    return (
      <div className="heart-icon" onClick={this.toggleModal}>
        <svg xmlns="http://www.w3.org/2000/svg" fill={fillColor} viewBox="0 0 24 24" stroke="currentColor">
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
          />
        </svg>
        {this.state.isClickedModal && !this.state.isFavourite && (
          <Modal
            header="Додати у обране?"
            closeButton={true}
            text="Ви точно хочете додати цей товар у обране?"
            onClose={this.onCloseModal}
            actions={
              <>
                <Button className="Button" text="Ok" onClick={this.addInFavourite} backgroundColor="#4a7c4a"></Button>
                <Button className="Button" text="Cancel" onClick={this.onCloseModal} backgroundColor="#893c3c"></Button>
              </>
            }
          />
        )}
        {this.state.isClickedModal && this.state.isFavourite && (
          <Modal
            header="Видалити з обраного?"
            closeButton={true}
            text="Ви точно хочете видалити цей товар з обраного?"
            onClose={this.onCloseModal}
            actions={
              <>
                <Button
                  className="Button"
                  text="Ok"
                  onClick={this.deleteFromFavourite}
                  backgroundColor="#4a7c4a"
                ></Button>
                <Button className="Button" text="Cancel" onClick={this.onCloseModal} backgroundColor="#893c3c"></Button>
              </>
            }
          />
        )}
      </div>
    );
  }
}

export default Heart;
