import React from 'react';
import { getGoods } from '../api/goods';
import ProductCard from './ProductCard';

export class Goods extends React.Component {
  constructor(props) {
    super();
    this.props = props;
    this.state = { goods: [] };
  }
  componentDidMount() {
    getGoods().then((r) => this.setState({ goods: r }));
  }
  render() {
    return (
      <div className="Goods-container container">
        {this.state.goods.map((good) => (
          <ProductCard
            key={good.article}
            name={good.name}
            price={good.price}
            image={good.image}
            color={good.color}
            article={good.article}
          />
        ))}
      </div>
    );
  }
}
