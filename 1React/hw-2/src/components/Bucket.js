import React, { Component } from 'react';
import { Modal } from './Modal';
import { Button } from './Button';
import { addItemInBucket } from '../api/addItemInBucket';

export class Bucket extends Component {
  constructor(props) {
    super(props);
    this.state = { isInBucket: false, isClickedModal: false };
    this.toggleModal = this.toggleModal.bind(this);
    this.addInBucket = this.addInBucket.bind(this);
  }

  toggleModal() {
    this.setState({ ...this.state, isClickedModal: !this.state.isClickedModal });
  }

  addInBucket() {
    this.setState({ ...this.state, isInBucket: true, isClickedModal: false });
    addItemInBucket(this.props.article);
  }

  componentDidMount() {
    const bucket = JSON.parse(localStorage.getItem('bucket'));
    if (bucket) {
      const isInBucket = bucket.includes(this.props.article);
      this.setState({ ...this.state, isInBucket });
    }
  }

  onCloseModal = () => {
    this.setState({ ...this.state, isClickedModal: false });
  };

  render() {
    return (
      <>
        <Button className="Button" text="Додати у кошик" backgroundColor="black" onClick={this.toggleModal} />
        {this.state.isClickedModal && (
          <Modal
            header="Додати у кошик?"
            closeButton={true}
            text="Ви точно хочете додати цей товар у кошик?"
            onClose={this.onCloseModal}
            actions={
              <>
                <Button className="Button" text="Ok" onClick={this.addInBucket} backgroundColor="#4a7c4a"></Button>
                <Button className="Button" text="Cancel" onClick={this.onCloseModal} backgroundColor="#893c3c"></Button>
              </>
            }
          />
        )}
      </>
    );
  }
}

export default Bucket;
