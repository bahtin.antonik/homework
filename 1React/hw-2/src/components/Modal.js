import React from 'react';
import { createPortal } from 'react-dom';
import PropTypes from 'prop-types';

export class Modal extends React.Component {
  static defaultProps = {
    className: 'Modal',
  };

  static propTypes = {
    backgroundColor: PropTypes.string,
    onClick: PropTypes.func,
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    actions: PropTypes.object,
  };

  render() {
    return createPortal(
      <div className="Modal-wrapper" onClick={this.props.onClose}>
        <div
          className="Modal"
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <div className="Modal-header">
            <h2>{this.props.header}</h2>
            {this.props.closeButton && (
              <button onClick={this.props.onClose} className="Modal-header-close-btn">
                X
              </button>
            )}
          </div>
          <p className="Modal-text">{this.props.text}</p>
          <div className="Modal-submit-btns">{this.props.actions}</div>
        </div>
      </div>,
      document.body
    );
  }
}
