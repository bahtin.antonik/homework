import React from 'react';
import { ReactComponent as BucketIcon } from '../images/bucket.svg';

export class Header extends React.Component {
  constructor() {
    super();

    this.state = { bucketArr: [], favouriteArr: [] };
  }

  mountFavouriteList() {
    const favouriteList = JSON.parse(localStorage.getItem('favourites'));
    if (favouriteList) {
      this.setState((prevState) => ({ ...prevState, favouriteArr: favouriteList }));
    }
  }

  mountBucketList() {
    const bucketList = JSON.parse(localStorage.getItem('bucket'));
    if (bucketList) {
      this.setState((prevState) => ({ ...prevState, bucketArr: bucketList }));
    }
  }

  componentDidMount() {
    this.mountBucketList();
    this.mountFavouriteList();
  }

  // componentDidUpdate(prevProps, prevState) {
  //   const bucketList = JSON.parse(localStorage.getItem('bucket'));
  //   if (bucketList.length !== prevState.bucketArr.length) {
  //     this.setState((prevState) => ({ ...prevState, bucketArr: bucketList }));
  //     console.log(this.state.bucketArr);
  //   }
  // }

  render() {
    return (
      <div className="Header container">
        <h1>Light Noir</h1>
        <div className="header-icons">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="red"
            height="32"
            width="32"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
            />
          </svg>
          <span>{this.state.favouriteArr.length}</span>
          <BucketIcon />
          <span>{this.state.bucketArr.length}</span>
        </div>
      </div>
    );
  }
}
