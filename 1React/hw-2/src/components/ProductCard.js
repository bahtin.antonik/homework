import React from 'react';
import Heart from './FavouriteHeart';
import { Bucket } from './Bucket';
import PropTypes from 'prop-types';

export default class ProductCard extends React.Component {
  static propTypes = {
    image: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    article: PropTypes.number.isRequired,
    price: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.props = props;
  }

  render() {
    return (
      <div className="product-card ">
        <a href="#" className="product-card-image">
          <img src={this.props.image} alt="image" />
          <span className="product-card-name">{this.props.name}</span>
          <Heart article={this.props.article} className="heart-icon" />
        </a>

        <span className="product-card-price">{this.props.price}</span>
        <span className="product-card-color">Колір: {this.props.color}</span>
        <Bucket article={this.props.article}></Bucket>
      </div>
    );
  }
}
