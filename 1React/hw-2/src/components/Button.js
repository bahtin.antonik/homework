import React from 'react';
import PropTypes from 'prop-types';

export class Button extends React.Component {
  static defaultProps = {
    className: 'Button',
  };

  static propTypes = {
    backgroundColor: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
  };

  render() {
    return (
      <button
        className={this.props.className}
        onClick={this.props.onClick}
        style={{ backgroundColor: this.props.backgroundColor }}
      >
        {this.props.text}
      </button>
    );
  }
}
