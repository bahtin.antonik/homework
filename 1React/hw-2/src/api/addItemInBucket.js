let bucket = [];

export function addItemInBucket(article) {
  const localBucket = JSON.parse(localStorage.getItem('bucket'));

  if (localBucket) {
    bucket = localBucket;
  }
  bucket.push(article);
  localStorage.setItem('bucket', JSON.stringify(bucket));
}
