let favourites = [];

export function deleteFromFavourite(article) {
  const localFavourites = JSON.parse(localStorage.getItem('favourites'));
  if (localFavourites) {
    favourites = localFavourites;
  }
  const newFavourites = favourites.filter((favourite) => {
    return favourite !== article;
  });
  localStorage.setItem('favourites', JSON.stringify(newFavourites));
}
