const btnCollection = document.querySelectorAll('.btn');
document.addEventListener('keypress', (event) => {
  btnCollection.forEach((btn) => {
    btn.classList.remove('btnPressed');
    if (event.key === btn.innerText) {
      btn.classList.add('btnPressed');
    } else if (event.key === btn.innerText.toLowerCase()) {
      btn.classList.add('btnPressed');
    }
  });
});
/*
Чому для роботи з input не рекомендується використовувати клавіатуру?
Через те що ввод даних може бути виконаний не тільки через клавіатуру, а й через
голосовий ввід або вставка через кнопки миші, тому треба це використовувати тільки
у тому випадку коли ми впевнені що дані будуть введені саме з клавіатури
*/
