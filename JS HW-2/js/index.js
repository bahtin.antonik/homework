let userName;
let userAge;
let result;

do {
  userName = prompt("Enter your name", userName);
} while (userName === "" || !userName);

do {
  userAge = Number(prompt("Enter your age", userAge));
} while (typeof userAge !== "number" || !userAge);

if (userAge >= 18 && userAge <= 22) {
  result = confirm("Are you sure you want to continue?");

  if (result) {
    alert(`Welcome ${userName}`);
  } else {
    alert("You are not allowed to visit this website.");
  }
} else if (userAge >= 22) {
  alert(`Welcome ${userName}`);
} else {
  alert("You are not allowed to visit this website.");
}

/* 
  Теоретичні питання

Які існують типи даних у Javascript?

number & BigInt & Boolean & String & null & undefined & object

У чому різниця між == і ===?

"==" - прирівнює  значення без суворого  приведення типів
"===" - прирівнює значення тільки з однаковим типом даних

Що таке оператор?

Оператор проводить операції з операндами.
Оператори бувають унарні, бінарні та тернарні.
приклади операторів: + - * / % > < => =< 

*/
