function createNewUser() {
  const firstName = prompt("What is your name?");
  const lastName = prompt("What is your last name?");

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    getLogin: function () {
      const lowercase = firstName.charAt(0) + lastName;
      const result = lowercase.toLowerCase();
      return result;
    },
  }
  return newUser;
};

const user = createNewUser()
console.log(user);
console.log(user.getLogin())

const user1 = createNewUser()
console.log(user1);
console.log(user1.getLogin())

/*
Теоретичні питання
1. Опишіть своїми словами, що таке метод об'єкту

Метод об'єкту це функція яка належить об'єкту 

2. Який тип даних може мати значення властивості об'єкта?

Будь який тип даних із 8 існуючих

3. Об'єкт це посилальний тип даних. Що означає це поняття?

Ми не можемо явно використовувати його, якщо намагатися присвоїти його дані то будуть присвоєні
не дані, а посилання на них.

*/